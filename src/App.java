import model.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(3.0f, 2.0f);
        System.out.println("Dien Tich HCN: " + rectangle2.getArea());
        System.out.println("Dien Tich HCN: " + rectangle2.getPerimeter());
        System.out.println("Dien Tich HCN: " + rectangle1.getArea());
        System.out.println("Dien Tich HCN: " + rectangle1.getPerimeter());
        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());

    }
}
